package com.flamecode.ploticalyproject;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.dom.Style;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.PWA;
import java.util.Locale;

/**
 * A sample Vaadin view class.
 * <p>
 * To implement a Vaadin view just extend any Vaadin component and
 * use @Route annotation to announce it in a URL as a Spring managed
 * bean.
 * Use the @PWA annotation make the application installable on phones,
 * tablets and some desktop browsers.
 * <p>
 * A new instance of this class is created for every new user and every
 * browser tab/window.
 */
@Route("loading")
@PWA(name = "Home of Doina Popa Project",
        shortName = "Home",
        description = "This is the test eg.",
        enableInstallPrompt = false)
public class MainView extends VerticalLayout {

    /**
     * Construct a new Vaadin view.
     * <p>
     * Build the initial UI state for the user accessing the application.
     */
    public MainView() {

        setMainProperties();
        // Use custom CSS classes to apply styling. This is defined in shared-styles.css.
        addADERLogo();
        addRandomPhrase();
    }

    private void addRandomPhrase() {

        RandomText randomText = new RandomText();
        Label label = new Label("\uD83C\uDFC6" + randomText.generateResult() + " \uD83C\uDFC6");
        Style style = label.getStyle();
        style.set("align-self", "center");
        style.set("font-family", "verdana");
        style.set("font-size", "100%");
        style.set("font-weight", "bold");
        add(label);
    }

    private void setMainProperties() {

        getStyle().set("justify-content", "center");
        setSizeFull();
        getStyle().set("background-color", "LightGray"); // set the background color
        UI.getCurrent().setLocale(new Locale("ro")); // set romain language
        UI.getCurrent().getPage().setTitle("Doina Popa | Încărcare"); // set title
    }

    private void addADERLogo() {

        String src = "https://firebasestorage.googleapis.com/v0/b/site-doina-popa.appspot.com/o/remove_background_moto.png?alt=media&token=30342af7-3a6c-41eb-b12a-f4e584d04a45";
        Image image = new Image();
        Style imageStyle = image.getStyle();
        imageStyle.set("align-self", "center");
        String percent = "24%";
        image.setHeight(percent);
        image.setWidth(percent);
        image.setSrc(src);
        image.addClickListener(
                imageClickEvent -> UI.getCurrent().navigate(MyAcasa.class));

        add(image); // adaugi elemente flow de tip html
    }
}
