package com.flamecode.ploticalyproject;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomText {

    private final Random random;
    private final List<String> list = new ArrayList<>();

    public RandomText() {

        random = new SecureRandom();
        list.add("Aici te poți înscrie în partid");
        list.add("De aici ne poți urmări deciziile și acțiunile");
        list.add("Vei găsi aici programul meu");
        list.add("Ultimele noutăți ale candidaturii mele");
    }

    public String generateResult(){

        return list.get(random.nextInt(list.size() - 1));
    }
}
