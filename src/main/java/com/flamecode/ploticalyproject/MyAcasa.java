package com.flamecode.ploticalyproject;

import com.flamecode.ploticalyproject.menu.MyDevinoSustinatorAcum;
import com.flamecode.ploticalyproject.menu.MyListaSustinatori;
import com.flamecode.ploticalyproject.menu.MyProgramDeSalvare;
import com.flamecode.ploticalyproject.menu.MySediu;
import com.vaadin.flow.component.*;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.templatemodel.TemplateModel;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import de.mekaso.vaadin.addons.Carousel;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * My Acasa version for desktop and tablet
 */
@Route("acasa")
@PageTitle("Doina Popa | Acasă")
@Tag("my-acasa")
@JsModule("./my-acasa.js")
public class MyAcasa extends PolymerTemplate<MyAcasa.MyAcasaModel> {

    @Id("header")
    private HorizontalLayout header;
    @Id("footer")
    private HorizontalLayout footer;
    @Id("facebook")
    private Image facebookImage;
    @Id("vaadinHorizontalLayout")
    private HorizontalLayout vaadinHorizontalLayoutHeader2;
    @Id("linkedin")
    private Image linkedinImage;
    @Id("vaadinVerticalLayout")
    private VerticalLayout vaadinVerticalLayout;
    @Id("h1")
    private H1 greetingMessage;

    /**
     * Creates a new MyAcasa.
     */
    public MyAcasa() {

        createMenuBar();
        addSocialLinks();
        createCarousel();
        setGreeting();

        // You can initialise any data required for the connected UI components here.
    }

    private void setGreeting() {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String format = dtf.format(now);
        format = format.substring(0, 2);
        int val = Integer.parseInt(format);

        if (val >= 5 && val < 12) {

            greetingMessage.setText("Bună Dimineața");
        } else if (val >= 12 && val <= 19) {

            greetingMessage.setText("Bună Ziua");
        } else {

            greetingMessage.setText("Bună Seara");
        }
    }

    private void createCarousel() {

        Carousel carousel = Carousel.create().withAutoplay().withDuration(3, TimeUnit.SECONDS);
        carousel.getElement().getStyle().set("margin", /*"var(--lumo-space-xl"*/ "2.185rem");
        List<String> list = new ArrayList<>();
        list.add("img/1cac5f6e-ecc9-4dd8-a52a-77ca37b8ddd6-min.jpg");
        list.add("img/IMG_20200813_174829-min.jpg");
        list.add("img/IMG_20200813_180448-min.jpg");
        for (String s : list) {

            Image image = new Image();
            image.setHeight("150%");
            image.setSrc(s);
            carousel.add(image);
        }

        vaadinVerticalLayout.add(carousel);
    }

    private void createMenuBar() {

        MenuBar menuBar = new MenuBar();
        menuBar.setOpenOnHover(true);

        menuBar.addItem("Programul De SALVARE", e -> UI.getCurrent().navigate(MyProgramDeSalvare.class));
        menuBar.addItem("Sediu", e -> UI.getCurrent().navigate(MySediu.class));

        MenuItem susținători = menuBar.addItem("Susținători");

        susținători.getSubMenu().addItem("Devino Susținător Acum",
                e -> UI.getCurrent().navigate(MyDevinoSustinatorAcum.class));
        susținători.getSubMenu().addItem("Listă Susținători",
                e -> UI.getCurrent().navigate(MyListaSustinatori.class));

        vaadinHorizontalLayoutHeader2.add(menuBar);
    }

    private void addSocialLinks() {

        facebookImage.addClickListener(
                imageClickEvent -> UI.getCurrent()
                        .getPage().open("https://www.facebook.com/DoinaPopaPrimar"));

        linkedinImage.addClickListener(
                imageClickEvent -> UI.getCurrent()
                        .getPage().open("https://www.linkedin.com/in/doina-popa-a455021b2/"));
    }

    /**
     * This model binds properties between MyAcasa and my-acasa
     */
    public interface MyAcasaModel extends TemplateModel {
        // Add setters and getters for template properties here.
    }
}
