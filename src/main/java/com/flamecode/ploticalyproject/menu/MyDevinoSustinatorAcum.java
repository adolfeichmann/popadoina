package com.flamecode.ploticalyproject.menu;

import com.vaadin.flow.templatemodel.TemplateModel;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;


@Tag("my-devino-sustinator-acum")
@JsModule("./my-devino-sustinator-acum.js")
public class MyDevinoSustinatorAcum extends PolymerTemplate<MyDevinoSustinatorAcum.MyDevinoSustinatorAcumModel> {


    public MyDevinoSustinatorAcum() {
        // You can initialise any data required for the connected UI components here.
    }

    /**
     * This model binds properties between MyDevinoSustinatorAcum and my-devino-sustinator-acum
     */
    public interface MyDevinoSustinatorAcumModel extends TemplateModel {
        // Add setters and getters for template properties here.
    }
}
