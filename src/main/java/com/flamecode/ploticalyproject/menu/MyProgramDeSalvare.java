package com.flamecode.ploticalyproject.menu;

import com.vaadin.flow.router.Route;
import com.vaadin.flow.templatemodel.TemplateModel;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;

@Route("program_de_salvare")
@Tag("my-program-de-salvare")
@JsModule("./my-program-de-salvare.js")
public class MyProgramDeSalvare extends PolymerTemplate<MyProgramDeSalvare.MyProgramDeSalvareModel> {


    public MyProgramDeSalvare() {
        // You can initialise any data required for the connected UI components here.
    }

    /**
     * This model binds properties between MyProgramDeSalvare and my-program-de-salvare
     */
    public interface MyProgramDeSalvareModel extends TemplateModel {
        // Add setters and getters for template properties here.
    }
}
