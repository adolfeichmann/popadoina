package com.flamecode.ploticalyproject.menu;

import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.templatemodel.TemplateModel;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;

@Route("sediu")
@Tag("my-sediu")
@JsModule("./my-sediu.js")
public class MySediu extends PolymerTemplate<MySediu.MySediuModel> {


    @Id("vaadinVerticalLayout")
    private VerticalLayout centerVerticalLayout;
    @Id("img")
    private Image img;
    @Id("img1")
    private Image img1;
    @Id("img2")
    private Image img2;
    @Id("img3")
    private Image img3;
    @Id("img4")
    private Image img4;
    @Id("img5")
    private Image img5;

    public MySediu() {

        addImages();
    }

    private void addImages() {

        Image img = this.img;
        img.setSrc("img/1cac5f6e-ecc9-4dd8-a52a-77ca37b8ddd6-min.jpg");
        img.getStyle().set("align-self", "center");
        img.setHeight("25%");

        img = this.img1;
        img.setSrc("img/1cac5f6e-ecc9-4dd8-a52a-77ca37b8ddd6-min.jpg");
        img.getStyle().set("align-self", "center");
        img.setHeight("25%");

        img = this.img2;
        img.setSrc("img/1cac5f6e-ecc9-4dd8-a52a-77ca37b8ddd6-min.jpg");
        img.getStyle().set("align-self", "center");
        img.setHeight("25%");

        img = this.img3;
        img.setSrc("img/1cac5f6e-ecc9-4dd8-a52a-77ca37b8ddd6-min.jpg");
        img.getStyle().set("align-self", "center");
        img.setHeight("25%");

        img = this.img4;
        img.setSrc("img/1cac5f6e-ecc9-4dd8-a52a-77ca37b8ddd6-min.jpg");
        img.getStyle().set("align-self", "center");
        img.setHeight("25%");

        img = this.img5;
        img.setSrc("img/1cac5f6e-ecc9-4dd8-a52a-77ca37b8ddd6-min.jpg");
        img.getStyle().set("align-self", "center");
        img.setHeight("25%");
    }

    /**
     * This model binds properties between MySediu and my-sediu
     */
    public interface MySediuModel extends TemplateModel {
        // Add setters and getters for template properties here.
    }
}
