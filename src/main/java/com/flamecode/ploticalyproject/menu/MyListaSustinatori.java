package com.flamecode.ploticalyproject.menu;

import com.vaadin.flow.templatemodel.TemplateModel;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;


@Tag("my-lista-sustinatori")
@JsModule("./my-lista-sustinatori.js")
public class MyListaSustinatori extends PolymerTemplate<MyListaSustinatori.MyListaSustinatoriModel> {


    public MyListaSustinatori() {
        // You can initialise any data required for the connected UI components here.
    }

    /**
     * This model binds properties between MyListaSustinatori and my-lista-sustinatori
     */
    public interface MyListaSustinatoriModel extends TemplateModel {
        // Add setters and getters for template properties here.
    }
}
