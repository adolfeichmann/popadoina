import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `my-devino-sustinator-acum`
 *
 * MyDevinoSustinatorAcum element.
 *
 * @customElement
 * @polymer
 */
class MyDevinoSustinatorAcum extends PolymerElement {

    static get template() {
        return html`
            <style include="shared-styles">
                :host {
                    display: block;
                    height: 100%;
                }
            </style>
        `;
    }

    static get is() {
        return 'my-devino-sustinator-acum';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(MyDevinoSustinatorAcum.is, MyDevinoSustinatorAcum);
