import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `my-program-de-salvare`
 *
 * MyProgramDeSalvare element.
 *
 * @customElement
 * @polymer
 */
class MyProgramDeSalvare extends PolymerElement {

    static get template() {
        return html`
            <style include="shared-styles">
                :host {
                    display: block;
                    height: 100%;
                }
            </style>
        `;
    }

    static get is() {
        return 'my-program-de-salvare';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(MyProgramDeSalvare.is, MyProgramDeSalvare);
