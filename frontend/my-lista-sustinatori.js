import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `my-lista-sustinatori`
 *
 * MyListaSustinatori element.
 *
 * @customElement
 * @polymer
 */
class MyListaSustinatori extends PolymerElement {

    static get template() {
        return html`
            <style include="shared-styles">
                :host {
                    display: block;
                    height: 100%;
                }
            </style>
        `;
    }

    static get is() {
        return 'my-lista-sustinatori';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(MyListaSustinatori.is, MyListaSustinatori);
