import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-ordered-layout/src/vaadin-scroller.js';
import '@vaadin/vaadin-ordered-layout/src/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-ordered-layout/src/vaadin-vertical-layout.js';

class MyAcasa extends PolymerElement {

    static get template() {
        return html`
<style include="shared-styles">
                :host {
                    display: block;
                    height: 100%;
                }
            </style>
<vaadin-vertical-layout style="width: 100%; height: 100%;">
 <vaadin-horizontal-layout class="header" style="width: 100%; flex-basis: var(--lumo-size-l); flex-shrink: 0; background-color: #f5f5f5; height: 100%; flex-wrap: wrap; flex-grow: 0; align-self: center; padding: var(--lumo-space-xs);" id="header" theme="spacing-xs">
  <vaadin-horizontal-layout style="flex-shrink: 1; flex-grow: 0; width: 45%;">
   <img id="aderLogo" style="align-self: center; width: 7%; max-height: 50; max-width: 50; padding: var(--lumo-space-xs); flex-grow: 0;" src="img/aderLogo.png">
   <label style="color: black; align-self: center; font-weight: bold; margin-right: var(--lumo-space-xl); padding-left: var(--lumo-space-s); font-size: 115%;">Doina Popa </label>
  </vaadin-horizontal-layout>
  <vaadin-horizontal-layout style="flex-shrink: 1; flex-grow: 0; width: 50%; flex-direction: row; justify-content: flex-end;" id="vaadinHorizontalLayout">
   <img id="facebook" style="flex-grow: 0; src: https://firebasestorage.googleapis.com/v0/b/site-doina-popa.appspot.com/o/aderLogo.png?alt=media&amp;token=1c543d73-142d-451e-a409-dc0d71adaffa; margin-bottom: var(--lumo-space-s); margin-left: var(--lumo-space-xl); width: 3.5%; align-self: center; padding-top: var(--lumo-space-s);" src="img/icon_facebook.png">
   <img style="align-self: center; margin: var(--lumo-space-s); margin-left: var(--lumo-space-l); width: 3.5%;" src="img/icon_linkedin.png" id="linkedin">
  </vaadin-horizontal-layout>
 </vaadin-horizontal-layout>
 <vaadin-vertical-layout style="width: 100%; flex-grow: 1; flex-shrink: 1; flex-basis: auto; align-self: center; align-items: center;" id="vaadinVerticalLayout">
  <vaadin-horizontal-layout class="content" style="flex-grow: 1; flex-shrink: 1; flex-basis: auto; background-color: white; justify-content: center;">
   <vaadin-vertical-layout style="align-self: center; align-items: center;">
    <h1 style="font-size: 300%; margin: var(--lumo-space-xs);" id="h1">Bună Ziua,</h1>
    <h1 style="margin: var(--lumo-space-xs); font-size: 300%;">Dragi Oneșteni!</h1>
   </vaadin-vertical-layout>
   <img style="align-self: center; flex-grow: 0;" src="img/poza_primary-removebg-preview.png">
  </vaadin-horizontal-layout>
  <h5 style="align-self: center; flex-grow: 0; flex-shrink: 1; font-size: 250%; margin-bottom: var(--lumo-space-l);">...</h5>
  <h2 style="margin-right: var(--lumo-space-xl); margin-left: var(--lumo-space-xl); margin-top: var(--lumo-space-xl); padding-right: var(--lumo-space-xl); padding-left: var(--lumo-space-xl); font-size: 180%;" id="h2">Aceasta este pagina mea personală unde veți putea descoperi noutăți din campania mea electorală, programul de SALVARE A ONEȘTIULUI, poți să mă susți direct și alte detalii. Vă mulțumesc! RATATA</h2>
 </vaadin-vertical-layout>
 <vaadin-horizontal-layout class="footer" style="width: 100%; flex-basis: var(--lumo-size-l); flex-shrink: 0; background-color: #C24B38;" id="footer">
  <vaadin-scroller style="width: 100%;">
   <vaadin-vertical-layout style="align-items: center; width: 100%;">
    <label style="font-weight: bold; margin-top: var(--lumo-space-xl);">Contact:</label>
    <label style="font-weight: bold;">Email: partiduladeronesti@gmail.com</label>
    <label style="font-weight: bold;">Adresa: Strada Mercur nr. 2, Onești, jud. Bacău</label>
    <label style="font-weight: bold;">Luni: 09:00 - 18:00 Marţi: 09:00 - 18:00 Miercuri: 09:00 - 18:00</label>
    <label style="font-weight: bold; padding-bottom: var(--lumo-space-xl);">Joi: 09:00 - 18:00 Vineri: 09:00 - 18:00 Sâmbătă: 11:00 - 18:00 Duminică: 11:00 - 18:00</label>
    <label style="align-self: center; padding-top: var(--lumo-space-l);">Copyright © 2020 PopaDoina. All rights reserved.</label>
   </vaadin-vertical-layout>
  </vaadin-scroller>
 </vaadin-horizontal-layout>
</vaadin-vertical-layout>
`;
    }

    static get is() {
        return 'my-acasa';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(MyAcasa.is, MyAcasa);
