import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-ordered-layout/src/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-ordered-layout/src/vaadin-vertical-layout.js';

class MySediu extends PolymerElement {

    static get template() {
        return html`
<style include="shared-styles">
                :host {
                    display: block;
                    height: 100%;
                }
            </style>
<vaadin-vertical-layout style="width: 100%; height: 100%;">
 <vaadin-horizontal-layout style="width: 100%; flex-grow: 0; flex-shrink: 1; flex-basis: auto;">
  <vaadin-vertical-layout class="content" style="flex-grow: 1; flex-shrink: 1; flex-basis: auto;" id="vaadinVerticalLayout">
   <h1 style="align-self: center; flex-shrink: 1;">Sediu ADER Onești</h1>
   <h1 style="margin-right: var(--lumo-space-xl); margin-left: var(--lumo-space-xl); margin-top: var(--lumo-space-xl); padding-right: var(--lumo-space-xl); padding-left: var(--lumo-space-xl); font-size: 180%;">Această pagină este destinată sediului partidului ADER din municipiul Onești. Aici veți găsi mai multe poze cu sediul nostru. Vă așteptăm!</h1>
   <img id="img">
   <img id="img1">
   <img id="img2">
   <img id="img3">
   <img id="img4">
   <img id="img5">
  </vaadin-vertical-layout>
 </vaadin-horizontal-layout>
 <vaadin-horizontal-layout class="footer" style="width: 100%; flex-basis: var(--lumo-size-l); flex-shrink: 1; background-color: #C24B38; flex-grow: 0;">
  <vaadin-scroller style="width: 100%;">
   <vaadin-vertical-layout style="align-items: center; width: 100%;">
    <label style="font-weight: bold; margin-top: var(--lumo-space-xl);">Contact:</label>
    <label style="font-weight: bold;">Email: partiduladeronesti@gmail.com</label>
    <label style="font-weight: bold;">Adresa: Strada Mercur nr. 2, Onești, jud. Bacău</label>
    <label style="font-weight: bold;">Luni: 09:00 - 18:00 Marți: 09:00 - 18:00 Miercuri: 09:00 - 18:00</label>
    <label style="font-weight: bold; padding-bottom: var(--lumo-space-xl);">Joi: 09:00 - 18:00 Vineri: 09:00 - 18:00 Sâmbătă: 11:00 - 18:00 Duminică: 11:00 - 18:00</label>
    <label style="align-self: center; padding-top: var(--lumo-space-l);">Copyright © 2020 PopaDoina. All rights reserved.</label>
   </vaadin-vertical-layout>
  </vaadin-scroller>
 </vaadin-horizontal-layout>
</vaadin-vertical-layout>
`;
    }

    static get is() {
        return 'my-sediu';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(MySediu.is, MySediu);
